package krowa;


public abstract class CowPath{

    /**
     * konstruktor domyślny
     */
    public CowPath(){
    }

    public void init(int cowPosition){
        this.cowPositionX = cowPosition;
        this.totalDistance = 0;
        this.positionX = 0;
        this.stepLength = 1;
        this.stepNo = 1;
        this.success = false;
    }

    public int getStepNo(){
        return stepNo;
    }

    public int getStepLength(){
        return stepLength;
    }

    public int getTotalDistance(){
        return totalDistance;
    }

    public boolean isSuccess(){
        return success;
    }

    public int getCowPositionX(){
        return cowPositionX;
    }

    public int getPositionX(){
        return positionX;
    }
    /* -------------------------------------- P R O T E C T E D ----------------------------------------------------- */

    /**
     * Numer kroku
     */
    protected int stepNo = 0;
    /**
     * Długość kroku
     */
    protected int stepLength = 1;
    /**
     * Całkowity pokonany dotąd dystans
     */
    protected int totalDistance = 0;
    /**
     * Miejsce, w którym aktualnie jesteśmy (x)
     */
    protected int positionX = 0;
    /**
     * Położenie krowy (x)
     */
    protected int cowPositionX = 0;
    /**
     * Czy znaleziono już krowę
     */
    protected boolean success = false;
}
