package krowa;

import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;


public abstract class CowPanel extends JPanel{

    /**
     * Szerokość okna (piksele)
     */
    public static final int SIZE_X = 1280;
    /**
     * Wysokość okna (piksele)
     */
    public static final int SIZE_Y = 700;

    /**
     * Konstruktor - wczytuje obrazek krowy.
     */
    public CowPanel(){
    }

    public int getUnitSize(){
        return unitSize;
    }

    public void setUnitSize(int unitSize){
        this.unitSize = unitSize;
    }

    public int getCowX(){
        return cowX;
    }

    public void setCowX(int cowX){
        this.cowX = cowX;
    }

    public Graphics2D getContext(){
        return gc;
    }

    public void setContext(Graphics2D gc){
        this.gc = gc;
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(image, null, 0, 0);
    }

    public void removeListener(){
        this.removeMouseListener(listener);
    }

    public abstract void makeMove(Move move);

    public abstract void initCowPanel();

    public abstract int getCowPositionXOnGrid();

    public abstract int getCowPositionYOnGrid();

    /* -------------------- P R O T E C T E D ------------------------------- */
    /**
     * Wielkość jednostki (piksele / jednostkę)
     */
    protected static final int PIXEL_PER_UNIT = 10;
    /**
     * Grubość linii, punktów itp w animacji
     */
    protected static int THICKNESS;
    /**
     * Czas uśpienia dla animacji
     */
    protected static final int SLEEP = 100;
    /**
     * Adres (względny) pliku z krowią grafiką - rozmiar MAŁY (dla 1D)
     */
    protected static final String IMG_SMALL_PATH = "/krowa/resources/cow2_small.png";
    /**
     * Adres (względny) pliku z krowią grafiką - rozmiar MNIEJSZY (dla 2D)
     */
    protected static final String IMG_SMALLER_PATH = "/krowa/resources/cow2_small.png";
    /**
     * Obrazek krowy
     */
    protected BufferedImage cowImg;
    /**
     * image - do rysowania po panelu
     */
    protected BufferedImage image;
    /**
     * graphic context panelu
     */
    protected Graphics2D gc;
    /**
     * kliknięta przez użytkownika pozycja krowy - x (współrzędne PANELU)
     */
    protected int cowX;
    /**
     * Rozmiar jednostki w pikselach
     */
    protected int unitSize = PIXEL_PER_UNIT;
    /**
     * Listener myszy, referencja na potrzeby usuwania.
     */
    protected MouseListener listener;

    /**
     * Inicjuje panel do rysowania
     */
    protected void init(){
        Dimension d = new Dimension(this.getWidth(), this.getHeight());
        setPreferredSize(d);
        setSize(d);
        image = new BufferedImage(getWidth(), getHeight(),
                BufferedImage.TYPE_INT_ARGB);
        gc = image.createGraphics();
        gc.setBackground(Color.white);
        gc.setColor(Color.white);
    }

}
