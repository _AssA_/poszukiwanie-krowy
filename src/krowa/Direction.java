package krowa;


/**
 * Kierunek ruchu dla 2D.
 */
public enum Direction{

    LEFT, RIGHT, UP, DOWN
}
