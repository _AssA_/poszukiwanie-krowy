package krowa;


public class CowPath2D extends CowPath{

    /**
     * Konstruktor domyślny
     */
    public CowPath2D(){
        super();
    }

    /**
     * Inicjalizacja (restart) algorytmu
     */
    public void init(int cowPositionX, int cowPositionY){
        super.init(cowPositionX);
        this.cowPositionY = cowPositionY;
        double rand = Math.random();
        if(rand < 0.25){
            direction = Direction.LEFT;
        }
        else if(rand < 0.5){
            direction = Direction.RIGHT;
        }
        else if(rand < 0.75){
            direction = Direction.UP;
        }
        else {
            direction = Direction.DOWN;
        }
    }

    /**
     * Wykonanie kolejnego kroku algorytmu.
     */
    public void step(){
        System.out.println("Position: [" + positionX + ", " + positionY + "]");
        if(success){
            return;
        }
        switch(direction){
            case LEFT:
                if(cowPositionY == positionY && cowPositionX <= positionX && cowPositionX >= positionX - stepLength){
                    positionX = cowPositionX;
                    totalDistance += Math.abs(cowPositionX - positionX);                    
                    success = true;
                    return;
                }
                positionX -= stepLength;
                direction = Direction.UP;
                break;
            case RIGHT:
                if(cowPositionY == positionY && cowPositionX >= positionX && cowPositionX <= positionX + stepLength){
                    positionX = cowPositionX;
                    totalDistance += Math.abs(cowPositionX - positionX);                    
                    success = true;
                    return;
                }
                positionX += stepLength;
                direction = Direction.DOWN;
                break;
            case UP:
                if(cowPositionX == positionX && cowPositionY >= positionY && cowPositionY <= positionY + stepLength){
                    positionY = cowPositionY;
                    totalDistance += Math.abs(cowPositionY - positionY);                    
                    success = true;
                    return;
                }
                positionY += stepLength;
                direction = Direction.RIGHT;
                break;
            case DOWN:
                if(cowPositionX == positionX && cowPositionY <= positionY && cowPositionY >= positionY - stepLength){
                    positionY = cowPositionY;
                    totalDistance += Math.abs(cowPositionY - positionY);                    
                    success = true;
                    return;
                }
                positionY -= stepLength;
                direction = Direction.LEFT;
                break;
        }
        if(stepNo % 2 == 0){
            stepLength++;
        }
        stepNo++;
        totalDistance += stepLength;
    }

    public int getPositionY(){
        return positionY;
    }

    public double getCowPositionY(){
        return cowPositionY;
    }

    public Direction getDirection(){
        return direction;
    }

    /* -------------------------------------- P R I V A T E --------------------------------------------------------- */
    /**
     * Miejsce, w którym aktualnie jesteśmy (y)
     */
    private int positionY = 0;
    /**
     * Położenie krowy (y)
     */
    private int cowPositionY = 0;
    /**
     * Czy idziemy prawo czy lewo
     */
    private Direction direction = Direction.RIGHT;
}
