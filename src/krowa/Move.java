package krowa;


public class Move{

    /**
     * Konstruktor dla ruchów jednowymiarowych
     */
    public Move(double from, double to){
        x_from = from;
        x_to = to;
    }

    /**
     * Konstruktor dla ruchów dwuwymiarowych
     */
    public Move(int x0, int y0, int x1, int y1, Direction dir){
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
        this.dir = dir;
        this.dist = Math.max(Math.abs(x1 - x0), Math.abs(y1 - y0));
    }

    public double getX_from(){
        return x_from;
    }

    public void setX_from(double x_from){
        this.x_from = x_from;
    }

    public double getX_to(){
        return x_to;
    }

    public void setX_to(double x_to){
        this.x_to = x_to;
    }

    public int getX0(){
        return x0;
    }

    public void setX0(int x0){
        this.x0 = x0;
    }

    public int getY0(){
        return y0;
    }

    public void setY0(int y0){
        this.y0 = y0;
    }

    public Direction getDir(){
        return dir;
    }

    public void setDir(Direction dir){
        this.dir = dir;
    }

    public int getDist(){
        return dist;
    }

    public void setDist(int dist){
        this.dist = dist;
    }

    public int getX1(){
        return x1;
    }

    public void setX1(int x1){
        this.x1 = x1;
    }

    public int getY1(){
        return y1;
    }

    public void setY1(int y1){
        this.y1 = y1;
    }

    /**
     * Pozycja początkowa (na osi, nie na panelu) krowy dla 1D
     */
    private double x_from;
    /**
     * Pozycja końcowa (na osi, nie na panelu) krowy dla 1D
     */
    private double x_to;
    /**
     * Wsp. X pozycji początkowej (na gridzie, nie na panelu) krowy 2D
     */
    private int x0;
    /**
     * Wsp. Y pozycji początkowej (na gridzie, nie na panelu) krowy 2D
     */
    private int y0;
    private int x1, y1;
    /**
     * Kierunek ruchu dla 2D
     */
    private Direction dir;
    /**
     * Odległość do pokonania (na gridzie, nie na panelu) dla 2D
     */
    private int dist;
}
