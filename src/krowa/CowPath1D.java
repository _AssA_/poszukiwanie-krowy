package krowa;


public class CowPath1D extends CowPath{

    /**
     * konstruktor domyślny
     */
    public CowPath1D(){
        super();
    }

    /**
     * Inicjalizacja (restart) algorytmu
     */
    @Override
    public void init(int cowPosition){
        super.init(cowPosition);
        direction = Math.random() < 0.5 ? 1 : -1;
    }

    /**
     * Wykonanie kolejnego kroku algorytmu.
     */
    public void step(){
        System.out.println("Position: " + positionX);
        System.out.println("stepLength: " + stepLength);
        if(success)
            return;
        if(stepNo % 2 == 1){
            if(cowPositionX < 0 && positionX + direction * stepLength <= cowPositionX
                    || cowPositionX > 0 && positionX + direction * stepLength >= cowPositionX
                    || cowPositionX == 0){
                success = true;
                totalDistance += Math.abs(cowPositionX - positionX);
                positionX = cowPositionX;
            }
            else {
                totalDistance += stepLength;
                positionX += direction * stepLength;
                stepNo++;
            }
        }
        else {
            totalDistance += stepLength;
            stepNo++;
            positionX = 0;
            stepLength *= 2;
            direction *= -1;
        }
    }

    public int getDirection(){
        return direction;
    }

    /* -------------------------------------- P R I V A T E --------------------------------------------------------- */
    /**
     * Czy idziemy prawo czy lewo
     */
    private int direction = 1;

   
}
