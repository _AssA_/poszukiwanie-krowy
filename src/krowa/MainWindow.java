package krowa;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.*;


public class MainWindow extends javax.swing.JFrame{

    public static final int D1 = 1;
    public static final int D2 = 2;

    public MainWindow(){
        setWindowsStyle();
        initComponents();
        initIcons();
        setCenterPosition();
        initPanel(D1);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel = new javax.swing.JPanel();
        dimButton = new javax.swing.JToggleButton();
        startButton = new javax.swing.JButton();
        newButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Poszukiwanie krowy");
        setIconImage(new ImageIcon(getClass().getResource("/krowa/resources/cow2_smaller.png")).getImage());
        setResizable(false);

        panel.setPreferredSize(new java.awt.Dimension(1280, 700));

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1280, Short.MAX_VALUE)
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 700, Short.MAX_VALUE)
        );

        dimButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        dimButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/krowa/resources/1D.png"))); // NOI18N
        dimButton.setText("WYMIAR");
        dimButton.setFocusable(false);
        dimButton.setIconTextGap(8);
        dimButton.setPreferredSize(new java.awt.Dimension(120, 40));
        dimButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dimButtonActionPerformed(evt);
            }
        });

        startButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        startButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/krowa/resources/start.png"))); // NOI18N
        startButton.setText("START");
        startButton.setFocusable(false);
        startButton.setPreferredSize(new java.awt.Dimension(120, 40));
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        newButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/krowa/resources/restart.png"))); // NOI18N
        newButton.setText("NOWA GRA");
        newButton.setFocusable(false);
        newButton.setPreferredSize(new java.awt.Dimension(120, 40));
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("KROK 1: Wybierz stopień trudności");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("KROK 2: Ustaw krowę");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("KROK 3: Rozpocznij poszukiwanie");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dimButton, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57)
                        .addComponent(jLabel2)
                        .addGap(63, 63, 63)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(newButton, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dimButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(newButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void setWindowsStyle(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    private void initIcons(){
        try {
            icon1D = new ImageIcon(getClass().getResource("/krowa/resources/1D.png"));
            icon2D = new ImageIcon(getClass().getResource("/krowa/resources/2D.png"));
        }
        catch(Exception ex){
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    private void setCenterPosition(){
        Dimension scrDim = Toolkit.getDefaultToolkit().getScreenSize();
        int w = (int) ((scrDim.getWidth() - getWidth()) / 2);
        int h = (int) ((scrDim.getHeight() - getHeight()) / 2);
        setLocation(w, h);
    }

    private void initPanel(int d){
        switch(d){
            case D1:
                cowPanel = new CowPanel1D();
                dim = 1;
                break;
            case D2:
                cowPanel = new CowPanel2D();
                dim = 2;
                break;
        }
        cowPanel.setSize(CowPanel.SIZE_X, CowPanel.SIZE_Y);
        cowPanel.initCowPanel();
        panel.removeAll();
        panel.add(cowPanel);
        panel.repaint();
    }

    private void end(int distance, int cow){
        String message = "Krowa znajdowała się w odległości: " + cow + ".\n";
        message += "Pokonany dystans to: " + distance + ".\n";
        message += "ALG / OPT = " + ((float) distance / cow);
        JOptionPane.showMessageDialog(null, message, "Znaleziono krowę!", JOptionPane.INFORMATION_MESSAGE);
        dimButton.setEnabled(true);
        newButton.setEnabled(true);
    }

    private void dimButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dimButtonActionPerformed
        if(dimButton.isSelected()){
            dimButton.setIcon(icon2D);
            initPanel(D2);
        }
        else {
            dimButton.setIcon(icon1D);
            initPanel(D1);
        }
    }//GEN-LAST:event_dimButtonActionPerformed

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        dimButton.setEnabled(false);
        startButton.setEnabled(false);
        newButton.setEnabled(false);
        cowPanel.removeListener();
        switch(dim){
            case D1:
                new Thread(new Play1D()).start();
                break;
            case D2:
                new Thread(new Play2D()).start();
                break;
        }

    }//GEN-LAST:event_startButtonActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        cowPanel.initCowPanel();
        dimButton.setEnabled(true);
        startButton.setEnabled(true);
    }//GEN-LAST:event_newButtonActionPerformed


    private class Play1D implements Runnable{

        @Override
        public void run(){
            CowPath1D path = new CowPath1D();
            path.init(cowPanel.getCowPositionXOnGrid());
            int old = 0;
            while(!path.isSuccess()){
                path.step();
                cowPanel.makeMove(new Move(old, path.getPositionX()));
                old = path.getPositionX();
            }
            end(path.getTotalDistance(), Math.abs(path.getCowPositionX()));
        }

    }


    private class Play2D implements Runnable{

        @Override
        public void run(){
            CowPath2D path = new CowPath2D();
            path.init(cowPanel.getCowPositionXOnGrid(), cowPanel.getCowPositionYOnGrid());
            int oldx = 0, oldy = 0;
            Direction olddir = path.getDirection();
            while(!path.isSuccess()){
                path.step();
                cowPanel.makeMove(new Move(oldx, oldy, path.getPositionX(), path.getPositionY(), olddir));
                oldx = path.getPositionX();
                oldy = path.getPositionY();
                olddir = path.getDirection();
            }
            end(path.getTotalDistance(), (int) Math.sqrt(Math.pow(path.getCowPositionX(), 2) + Math.pow(path.getCowPositionY(), 2)));
        }

    }
    private ImageIcon icon1D, icon2D;
    private CowPanel cowPanel;
    private int dim;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton dimButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton newButton;
    private javax.swing.JPanel panel;
    private javax.swing.JButton startButton;
    // End of variables declaration//GEN-END:variables
}
