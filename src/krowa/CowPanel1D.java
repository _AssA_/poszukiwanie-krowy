package krowa;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;
import javax.imageio.ImageIO;


public class CowPanel1D extends CowPanel {

    public CowPanel1D() {
        try {
            cowImg = ImageIO.read(getClass().getResource(IMG_SMALL_PATH));
        }
        catch(Exception ex){
            ex.printStackTrace();
            System.exit(-1);
        }
        THICKNESS = 4;
    }

    /**
     * Inicjalizacja (RESET) panelu
     */
    @Override
    public void initCowPanel() {
        init();
        listener = new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                cowX = e.getX();
                drawAxis();
                putCow();
            }
        };
        this.addMouseListener(listener);

        Random rand = new Random();
        cowX = rand.nextInt(SIZE_X);
        drawAxis();
        putCow();
    }

    /**
     * Przekształca współrzędną X krowy na panelu (wylosowaną lub wyklikaną)
     * na współrzędne krowy na osi (planszy).
     */
    @Override
    public int getCowPositionXOnGrid() {
        return (int) Math.round((1.0 * cowX - SIZE_X / 2) / (1.0 * unitSize));
    }

    /**
     * Przedstawienie (animacja) jednego kroku krowoszukacza na planszy
     * x_from - pozycja (NA PLANSZY) początkowa krowoszukacza 
     * x_to - pozycja (NA PLANSZY) końcowa krowoszukacza
     */
    @Override
    public void makeMove(Move move) {
        int x0 = (int) (unitSize * move.getX_from() + SIZE_X / 2);
        int x1 = (int) (unitSize * move.getX_to() + SIZE_X / 2);
        int squareSize = 2 * THICKNESS + 1;
        int step = (x1 < x0 ? -squareSize : squareSize);
        int y0 = SIZE_Y / 2 - THICKNESS;
        int dist = Math.abs(x1 - x0) / squareSize;
        int rem = Math.abs(x1 - x0) - dist * squareSize;
        if(x1 < x0){
            rem = -rem;
            System.out.println("rem: " + rem);
            x0 -= squareSize;
        }
        // Rysowanie (animowane) jednego kroku krowoszukacza
        gc.setColor(Color.red);
        gc.fillRect(x0, y0, squareSize, squareSize);
        repaint();
        try {
            Thread.sleep(SLEEP);
        }
        catch(InterruptedException ex) {}
        for(int i = 1; i < dist; ++i) {
            gc.setColor(Color.green);
            gc.fillRect(x0, y0, squareSize, squareSize);
            x0 += step;
            gc.setColor(Color.red);
            gc.fillRect(x0, y0, squareSize, squareSize);
            repaint();
            try {
                Thread.sleep(SLEEP);
            }
            catch(InterruptedException ex) {}
        }
        gc.setColor(Color.green);
        gc.fillRect(x0, y0, squareSize, squareSize);
        repaint();
        if(rem != 0) {
            x0 += rem;
            gc.setColor(Color.red);
            gc.fillRect(x0, y0, squareSize, squareSize);
            repaint();
            try {
                Thread.sleep(SLEEP);
            }
            catch(InterruptedException ex) { }
            gc.setColor(Color.green);
            gc.fillRect(x0, y0, squareSize, squareSize);
            repaint();
        }
    }
    
    @Override
    public int getCowPositionYOnGrid() {
        throw new UnsupportedOperationException("Not supported.");
    }

    /* -------------------------------------- P R I V A T E --------------------------------------------------------- */
    /**
     * Narysowanie osi X
     */
    private void drawAxis() {
        gc.clearRect(0, 0, getWidth(), getHeight());
        int x0 = SIZE_X / 2;
        int y0 = SIZE_Y / 2;
        int step = 10 * unitSize;
        gc.setColor(Color.lightGray);
        for(int i = 0; i < SIZE_X; i += unitSize) {
            if(Math.abs(x0 - i) % step == 0){
                gc.drawLine(i, y0 - 2 * (THICKNESS + 2), i, y0 + 2 * (THICKNESS + 2));
            }
            else {
                gc.drawLine(i, y0 - (THICKNESS + 2), i, y0 + (THICKNESS + 2));
            }
        }
        gc.setColor(Color.magenta);
        gc.setStroke(new BasicStroke(1.5f));
        gc.drawLine(0, y0, SIZE_X, y0);
        gc.drawLine(x0, 0, x0, SIZE_Y);
        gc.setStroke(new BasicStroke());
        repaint();
    }

    /**
     * Umieszczenie krowy na planszy Wersja do wywołania z parametem będącym WSPÓŁRZĘDNĄ KROWY NA OSI Wstawia obrazek i
     * prostokącik czerwony
     *
     */
    private void putCow(double cowOnAxisX) {
        cowX = (int) (unitSize * cowOnAxisX + SIZE_X / 2);
        putCow();
    }

    /**
     * Umieszczenie krowy na planszy Wstawia obrazek i prostokącik czerwony
     */
    private void putCow() {
        gc.setColor(Color.blue);
        int cowImgX;
        if(cowX < cowImg.getWidth() / 2){
            cowImgX = 0;
        }
        else if(SIZE_X - cowX < cowImg.getWidth() / 2){
            cowImgX = SIZE_X - cowImg.getWidth();
        }
        else {
            cowImgX = cowX - cowImg.getWidth() / 2;
        }
        gc.fillRect(cowX - THICKNESS, SIZE_Y / 2 - THICKNESS,
                2 * THICKNESS + 1, 2 * THICKNESS + 1);
        gc.drawImage(cowImg, null, cowImgX,
                SIZE_Y / 2 - (cowImg.getHeight() + 5));
        repaint();
    }
    
}
