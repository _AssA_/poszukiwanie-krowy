package krowa;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;
import javax.imageio.ImageIO;


public class CowPanel2D extends CowPanel{

    public CowPanel2D(){
        try {
            cowImg = ImageIO.read(getClass().getResource(IMG_SMALL_PATH));
        }
        catch(Exception ex){
            ex.printStackTrace();
            System.exit(-1);
        }
        THICKNESS = 2;
    }

    public int getCowY(){
        return cowY;
    }

    public void setCowY(int cowY){
        this.cowY = cowY;
    }

    /**
     * Inicjalizacja (RESET) panelu
     */
    @Override
    public void initCowPanel(){
        init();
        listener = new MouseAdapter(){
            @Override
            public void mouseReleased(MouseEvent e){
                cowX = roundToGrid(e.getX());
                cowY = roundToGrid(e.getY());
                drawGrid();
                putCow();
            }

        };
        this.addMouseListener(listener);

        Random rand = new Random();
        cowX = roundToGrid(rand.nextInt(SIZE_X));
        cowY = roundToGrid(rand.nextInt(SIZE_Y));
        drawGrid();
        putCow();
    }

    /**
     * Przekształca współrzędną X krowy na na współrzędne krowy na gridzie (planszy).
     */
    @Override
    public int getCowPositionXOnGrid(){
        return (int) Math.round((cowX - SIZE_X / 2) / unitSize);
    }

    /**
     * Przekształca współrzędną Y krowy na panelu na współrzędne krowy na gridzie (planszy).
     */
    @Override
    public int getCowPositionYOnGrid(){
        return (int) Math.round((SIZE_Y / 2 - cowY) / unitSize);
    }

    /**
     * Przedstawienie (animacja) jednego kroku krowoszukacza na planszy x0, y0 - pozycja (NA PLANSZY) początkowa
     * krowoszukacza dir - kierunek ruchu krowoszukacza dist - odległość do przejścia dla krowoszukacza
     */
    @Override
    public void makeMove(Move move){
        int x0 = (int) (unitSize * move.getX0() + SIZE_X / 2);
        int y0 = (int) (SIZE_Y / 2 - unitSize * move.getY0());
        int squareSize = 2 * THICKNESS + 1;
        int stepSize = squareSize;
        // Liczba pełnych "kroków" czerwonego punktu - krowoszukacza
        int steps = move.getDist() * unitSize / squareSize;
        // Rozmiar niepełnego kroku
        int rem = move.getDist() * unitSize - steps * squareSize;
        switch(move.getDir()){
            case LEFT:
                stepSize = -stepSize;
                x0 -= squareSize;
                y0 -= THICKNESS;
                rem = -rem;
                break;
            case RIGHT:
                y0 -= THICKNESS;
                break;
            case UP:
                stepSize = -squareSize;
                x0 -= THICKNESS;
                y0 -= squareSize;
                rem = -rem;
                break;
            case DOWN:
                x0 -= THICKNESS;
                break;
        }
        // Rysowanie (animowane) jednego kroku krowoszukacza
        gc.setColor(Color.red);
        gc.fillRect(x0, y0, squareSize, squareSize);
        repaint();
        try {
            Thread.sleep(SLEEP);
        }
        catch(InterruptedException ex){
        }
        for(int i = 1; i < steps; ++i){
            gc.setColor(Color.green);
            gc.fillRect(x0, y0, squareSize, squareSize);
            switch(move.getDir()){
                case LEFT:
                case RIGHT:
                    x0 += stepSize;
                    break;
                case UP:
                case DOWN:
                    y0 += stepSize;
                    break;
            }
            gc.setColor(Color.red);
            gc.fillRect(x0, y0, squareSize, squareSize);
            repaint();
            try {
                Thread.sleep(SLEEP);
            }
            catch(InterruptedException ex){
            }
        }
        gc.setColor(Color.green);
        gc.fillRect(x0, y0, squareSize, squareSize);
        repaint();
        if(rem != 0){
            switch(move.getDir()){
                case LEFT:
                case RIGHT:
                    x0 += rem;
                    break;
                case UP:
                case DOWN:
                    y0 += rem;
                    break;
            }
            gc.setColor(Color.red);
            gc.fillRect(x0, y0, squareSize, squareSize);
            repaint();
            try {
                Thread.sleep(SLEEP);
            }
            catch(InterruptedException ex){
            }
            gc.setColor(Color.green);
            gc.fillRect(x0, y0, squareSize, squareSize);
            repaint();
        }
    }

    /* ------------------- P R I V A T E ------------------------------------ */
    /**
     * Odrysowanie siatki - domyślna jednostka 10px / unit
     */
    private void drawGrid(){
        gc.clearRect(0, 0, getWidth(), getHeight());
        int x0 = SIZE_X / 2;
        int y0 = SIZE_Y / 2;
        // Cała siatka
        gc.setColor(Color.lightGray);
        for(int i = 0; i < SIZE_X; i += unitSize){
            gc.drawLine(i, 0, i, SIZE_Y);
        }
        for(int i = 0; i < SIZE_Y; i += unitSize){
            gc.drawLine(0, i, SIZE_X, i);
        }
        // Główne osie OX i OY
        gc.setColor(Color.magenta);
        gc.setStroke(new BasicStroke(2.0f));
        gc.drawLine(0, y0, SIZE_X, y0);
        gc.drawLine(x0, 0, x0, SIZE_Y);
        gc.setStroke(new BasicStroke());
        repaint();
    }

    /**
     * Umieszczenie krowy na planszy Wstawia obrazek i prostokącik czerwony
     */
    private void putCow(){
        gc.setColor(Color.blue);
        int hOffset = 10;
        int cowImgX, cowImgY;
        // obliczenie właściwego cowImgX - wsp. X LU rogu obrazka krowy
        if(cowX < cowImg.getWidth() / 2){
            cowImgX = 0;
        }
        else if(SIZE_X - cowX < cowImg.getWidth() / 2){
            cowImgX = SIZE_X - cowImg.getWidth();
        }
        else if(cowX < SIZE_X / 2){
            cowImgX = cowX - cowImg.getWidth();
        }
        else {
            cowImgX = cowX + 1;
        }
        // obliczenie właściwego cowImgY - wsp. Y LU rogu obrazka krowy
        if((cowY < SIZE_Y / 2 && cowY >= hOffset + cowImg.getHeight())
                || (SIZE_Y - cowY < hOffset + cowImg.getHeight())){
            cowImgY = cowY - (hOffset + cowImg.getHeight());
        }
        else {
            cowImgY = cowY + hOffset;
        }
        // Odrysowanie punktu na pozycji krowy i obrazka
        gc.fillRect(cowX - THICKNESS, cowY - THICKNESS,
                2 * THICKNESS + 1, 2 * THICKNESS + 1);
        gc.drawImage(cowImg, null, cowImgX, cowImgY);
        repaint();
    }

    /**
     * Umieszczenie krowy na planszy Wersja do wywołania z parametrami będącymi WSPÓŁRZĘDNYMI KROWY NA GRIDZIE (nie na
     * panelu) Wstawia obrazek i prostokącik czerwony.
     *
     * Metoda "w razie czego", raczej ustawić cowX i cowY i używać putCow()
     *
     */
    private void putCow(int cowOnGridX, int cowOnGridY){
        setCowX(unitSize * cowOnGridX + SIZE_X / 2);
        setCowY(-unitSize * cowOnGridY + SIZE_Y / 2);
        putCow();
    }

    /**
     * Zaokrąglanie wylosowanej lub wyklikanej współrzędnej krowy NA PANELU (x lub y) do punktu kratowego.
     */
    private int roundToGrid(int coord){
        int rem = coord % unitSize;
        if(rem < unitSize / 2){
            coord -= rem;
        }
        else {
            coord += (unitSize - rem);
        }
        return coord;
    }

    private int cowY;
}
